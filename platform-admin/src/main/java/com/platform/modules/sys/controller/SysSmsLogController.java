/*
 *
 *      Copyright (c) 2018-2099, lipengjun All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the fly2you.cn developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lipengjun (939961241@qq.com)
 *
 */
package com.platform.modules.sys.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.platform.common.annotation.SysLog;
import com.platform.common.utils.Constant;
import com.platform.common.utils.RestResponse;
import com.platform.modules.sys.entity.SmsConfig;
import com.platform.modules.sys.entity.SysSmsLogEntity;
import com.platform.modules.sys.service.SysConfigService;
import com.platform.modules.sys.service.SysSmsLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 短信发送日志Controller
 *
 * @author 李鹏军
 * @since 2019-02-12 09:51:15
 */
@RestController
@RequestMapping("sys/smslog")
@Api(tags = {"短信发送日志-管理后台"})
public class SysSmsLogController extends AbstractController {
    @Autowired
    private SysSmsLogService sysSmsLogService;
    @Autowired
    private SysConfigService sysConfigService;

    /**
     * 查看所有列表
     *
     * @param params 查询参数
     * @return RestResponse
     */
    @GetMapping("/queryAll")
    @RequiresPermissions("sys:smslog:list")
    @ApiOperation(value = "查看所有列表")
    public RestResponse queryAll(@RequestParam Map<String, Object> params) {
        List<SysSmsLogEntity> list = sysSmsLogService.queryAll(params);

        return RestResponse.success().put("list", list);
    }

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return RestResponse
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:smslog:list")
    @ApiOperation(value = "分页查询")
    public RestResponse list(@RequestParam Map<String, Object> params) {
        Page page = sysSmsLogService.queryPage(params);

        return RestResponse.success().put("page", page);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return RestResponse
     */
    @GetMapping("/info/{id}")
    @RequiresPermissions("sys:smslog:info")
    @ApiOperation(value = "根据主键查询详情")
    public RestResponse info(@PathVariable("id") String id) {
        SysSmsLogEntity sysSmsLog = sysSmsLogService.getById(id);

        return RestResponse.success().put("smslog", sysSmsLog);
    }

    /**
     * 保存
     *
     * @param sysSmsLog sysSmsLog
     * @return RestResponse
     */
    @SysLog("保存短信发送记录")
    @PostMapping("/save")
    @RequiresPermissions("sys:smslog:save")
    @ApiOperation(value = "保存短信发送记录")
    public RestResponse save(@RequestBody SysSmsLogEntity sysSmsLog) {

        sysSmsLogService.add(sysSmsLog);

        return RestResponse.success();
    }

    /**
     * 修改
     *
     * @param sysSmsLog sysSmsLog
     * @return RestResponse
     */
    @SysLog("修改短信发送记录")
    @PostMapping("/update")
    @RequiresPermissions("sys:smslog:update")
    @ApiOperation(value = "修改短信发送记录")
    public RestResponse update(@RequestBody SysSmsLogEntity sysSmsLog) {

        sysSmsLogService.update(sysSmsLog);

        return RestResponse.success();
    }

    /**
     * 删除
     *
     * @param ids ids
     * @return RestResponse
     */
    @SysLog("删除短信发送记录")
    @PostMapping("/delete")
    @RequiresPermissions("sys:smslog:delete")
    @ApiOperation(value = "删除短信发送记录")
    public RestResponse delete(@RequestBody String[] ids) {
        sysSmsLogService.deleteBatch(ids);

        return RestResponse.success();
    }

    /**
     * 短信配置信息
     *
     * @return RestResponse
     */
    @GetMapping("/config")
    @RequiresPermissions("sys:smslog:config")
    @ApiOperation(value = "短信配置信息")
    public RestResponse config() {
        SmsConfig config = sysConfigService.getConfigObject(Constant.SMS_CONFIG_KEY, SmsConfig.class);

        return RestResponse.success().put("config", config);
    }

    /**
     * 保存短信配置信息
     *
     * @param config config
     * @return RestResponse
     */
    @SysLog("保存短信配置信息")
    @RequiresPermissions("sys:smslog:config")
    @PostMapping("/saveConfig")
    @ApiOperation(value = "保存短信配置信息")
    public RestResponse saveConfig(@RequestBody SmsConfig config) {
        sysConfigService.updateValueByKey(Constant.SMS_CONFIG_KEY, JSON.toJSONString(config));
        return RestResponse.success();
    }
}
