/*
 *
 *      Copyright (c) 2018-2099, lipengjun All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the fly2you.cn developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lipengjun (939961241@qq.com)
 *
 */
package com.platform.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.platform.common.annotation.SysLog;
import com.platform.common.utils.RestResponse;
import com.platform.common.validator.ValidatorUtils;
import com.platform.common.validator.group.AddGroup;
import com.platform.common.validator.group.UpdateGroup;
import com.platform.modules.sys.entity.SysDictEntity;
import com.platform.modules.sys.entity.SysDictGroupEntity;
import com.platform.modules.sys.service.SysDictGroupService;
import com.platform.modules.sys.service.SysDictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * 数据字典Controller
 *
 * @author 李鹏军
 * @since 2019-01-15 11:42:20
 */
@RestController
@RequestMapping("sys/dict")
@Api(tags = {"数据字典-管理后台"})
public class SysDictController {
    @Autowired
    private SysDictService sysDictService;
    @Autowired
    private SysDictGroupService sysDictGroupService;

    /**
     * 查看所有列表
     *
     * @param params 查询参数
     * @return RestResponse
     */
    @GetMapping("/queryAll")
    @RequiresPermissions("sys:dict:list")
    @ApiOperation(value = "查看所有列表")
    public RestResponse queryAll(@RequestParam Map<String, Object> params) {
        List<SysDictEntity> list = sysDictService.queryAll(params);

        return RestResponse.success().put("list", list);
    }

    /**
     * 分页查询
     *
     * @param params 查询参数
     * @return RestResponse
     */
    @GetMapping("/list")
    @RequiresPermissions("sys:dict:list")
    @ApiOperation(value = "分页查询")
    public RestResponse list(@RequestParam Map<String, Object> params) {
        IPage page = sysDictService.queryPage(params);

        return RestResponse.success().put("page", page);
    }

    /**
     * 根据主键查询详情
     *
     * @param id 主键
     * @return RestResponse
     */
    @GetMapping("/info/{id}")
    @RequiresPermissions("sys:dict:info")
    @ApiOperation(value = "根据主键查询详情")
    public RestResponse info(@PathVariable("id") String id) {
        SysDictEntity sysDict = sysDictService.getById(id);

        return RestResponse.success().put("dict", sysDict);
    }

    /**
     * 保存
     *
     * @return RestResponse
     */
    @SysLog("保存数据字典")
    @PostMapping("/save")
    @RequiresPermissions("sys:dict:save")
    @ApiOperation(value = "保存")
    public RestResponse save(@RequestBody SysDictEntity sysDict) {
        ValidatorUtils.validateEntity(sysDict, AddGroup.class);
        sysDictService.add(sysDict);

        return RestResponse.success();
    }

    /**
     * 修改
     *
     * @return RestResponse
     */
    @SysLog("修改数据字典")
    @PostMapping("/update")
    @RequiresPermissions("sys:dict:update")
    @ApiOperation(value = "修改")
    public RestResponse update(@RequestBody SysDictEntity sysDict) {
        ValidatorUtils.validateEntity(sysDict, UpdateGroup.class);
        sysDictService.update(sysDict);

        return RestResponse.success();
    }

    /**
     * 删除
     *
     * @param ids ids
     * @return RestResponse
     */
    @SysLog("删除数据字典")
    @PostMapping("/delete")
    @RequiresPermissions("sys:dict:delete")
    @ApiOperation(value = "删除")
    public RestResponse delete(@RequestBody String[] ids) {
        sysDictService.deleteBatch(ids);

        return RestResponse.success();
    }

    /**
     * 根据code查询数据字典
     *
     * @param params 查询参数
     * @return RestResponse
     */
    @GetMapping("/queryByCode")
    @ApiOperation(value = "根据code查询数据字典")
    public RestResponse queryByCode(@RequestParam Map<String, Object> params) {
        String code = (String) params.get("code");
        SysDictGroupEntity sysDictGroupEntity = sysDictGroupService.getOne(new QueryWrapper<SysDictGroupEntity>()
                .eq(StringUtils.isNotBlank(code), "code", code)
        );
        String type = "";
        if (null != sysDictGroupEntity) {
            type = sysDictGroupEntity.getName();
        }

        List<SysDictEntity> list = sysDictService.queryByCode(params);

        return RestResponse.success().put("list", list).put("type", type);
    }
}
