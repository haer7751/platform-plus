/*
 *
 *      Copyright (c) 2018-2099, lipengjun All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the fly2you.cn developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lipengjun (939961241@qq.com)
 *
 */

package com.platform.modules.app.controller;

import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaPhoneNumberInfo;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.platform.annotation.IgnoreAuth;
import com.platform.annotation.LoginUser;
import com.platform.common.utils.RestResponse;
import com.platform.common.utils.StringUtils;
import com.platform.common.validator.AbstractAssert;
import com.platform.modules.app.entity.FullUserInfo;
import com.platform.modules.wx.entity.WxUserEntity;
import com.platform.modules.wx.service.WxUserService;
import com.platform.utils.JwtUtils;
import io.swagger.annotations.*;
import lombok.extern.slf4j.Slf4j;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.bean.WxOAuth2UserInfo;
import me.chanjar.weixin.common.bean.oauth2.WxOAuth2AccessToken;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * APP登录授权
 *
 * @author 李鹏军
 */
@Slf4j
@RestController
@RequestMapping("/app/auth")
@Api(tags = "AppLoginController|APP登录接口")
public class AppLoginController extends AppBaseController {
    private static final String USER_INFO = "userInfo";
    @Autowired
    private WxUserService userService;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private WxMaService wxMaService;
    @Autowired
    private WxMpService wxMpService;

    /**
     * 微信小程序登录
     */
    @IgnoreAuth
    @PostMapping("LoginByMa")
    @ApiOperation(value = "微信小程序登录", notes = "wx.login()每次返回的code只能使用一次")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", name = "jsonObject", value = "JSON格式参数", examples = @Example({
                    @ExampleProperty(mediaType = "code", value = "oxaA11ulr9134oBL9Xscon5at_Gc"),
                    @ExampleProperty(mediaType = "userInfo", value = "wx.login()返回的userInfo信息，JSON格式参数")
            }), required = true, dataType = "string")
    })
    public RestResponse loginByMa(@RequestBody JSONObject jsonObject) {
        FullUserInfo fullUserInfo = null;
        String code = jsonObject.getString("code");
        AbstractAssert.isBlank(code, "登录失败：code为空");

        if (null != jsonObject.get(USER_INFO)) {
            fullUserInfo = jsonObject.getObject(USER_INFO, FullUserInfo.class);
        }
        AbstractAssert.isNull(fullUserInfo, "登录失败：userInfo为空");

        try {
            WxMaJscode2SessionResult session = wxMaService.getUserService().getSessionInfo(code);
            // 用户信息校验
            log.info("》》》微信返回sessionData：" + session.toString());

            if (!wxMaService.getUserService().checkUserInfo(session.getSessionKey(), fullUserInfo.getRawData(), fullUserInfo.getSignature())) {
                log.error("登录失败：数据签名验证失败");
                return RestResponse.error("登录失败");
            }

            // 解密用户信息
            WxMaUserInfo wxMpUser = wxMaService.getUserService().getUserInfo(session.getSessionKey(), fullUserInfo.getEncryptedData(), fullUserInfo.getIv());

            WxUserEntity user = userService.getOne(new QueryWrapper<WxUserEntity>().eq("UNION_ID", session.getUnionid()));
            if (user == null) {
                user = userService.getOne(new QueryWrapper<WxUserEntity>().eq("MA_OPENID", session.getOpenid()));
            }
            // 没有数据即新注册用户
            if (user == null) {
                user = new WxUserEntity();
                user.setAllIntegral(0);
                user.setIntegral(0);
                user.setRegisterTime(new Date());
                user.setRegisterIp(getClientIp());
            }
            user.setLastLoginTime(new Date());
            user.setLastLoginIp(getClientIp());
            user.setUnionId(session.getUnionid());
            user.setMaOpenid(session.getOpenid());
            //性别 0：未知、1：男、2：女
            user.setSex(Integer.parseInt(wxMpUser.getGender()));
            user.setNickname(wxMpUser.getNickName());
            user.setHeadImgUrl(wxMpUser.getAvatarUrl());
            user.setCity(wxMpUser.getCity());
            user.setProvince(wxMpUser.getProvince());
            userService.saveOrUpdate(user);

            String token = jwtUtils.generateToken(user.getId());

            if (StringUtils.isNullOrEmpty(token)) {
                log.error("登录失败：token生成异常");
                return RestResponse.error("登录失败");
            }
            long expire = System.currentTimeMillis() + jwtUtils.getExpire();
            return RestResponse.success().put("token", token).put("userInfo", user).put("userId", user.getId()).put("expire", expire);
        } catch (WxErrorException e) {
            log.error("登录失败：" + e.getMessage());
            return RestResponse.error("登录失败");
        }
    }

    /**
     * 微信小程序手机号授权
     */
    @IgnoreAuth
    @PostMapping("LoginByMaPhone")
    @ApiOperation(value = "微信小程序手机号授权", notes = "wx.login()每次返回的code只能使用一次")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", name = "jsonObject", value = "JSON格式参数", examples = @Example({
                    @ExampleProperty(mediaType = "code", value = "oxaA11ulr9134oBL9Xscon5at_Gc"),
                    @ExampleProperty(mediaType = "userInfo", value = "wx.login()返回的userInfo信息，JSON格式参数")
            }), required = true, dataType = "string")
    })
    public RestResponse loginByMaPhone(@RequestBody JSONObject jsonObject) {
        FullUserInfo fullUserInfo = null;
        String code = jsonObject.getString("code");
        AbstractAssert.isBlank(code, "手机号授权失败：code为空");
        String userId = jsonObject.getString("userId");
        AbstractAssert.isBlank(userId, "userId为空");

        if (null != jsonObject.get(USER_INFO)) {
            fullUserInfo = jsonObject.getObject(USER_INFO, FullUserInfo.class);
        }
        AbstractAssert.isNull(fullUserInfo, "手机号授权失败：userInfo为空");

        try {
            WxMaJscode2SessionResult session = wxMaService.getUserService().getSessionInfo(code);
            // 用户信息校验
            log.info("》》》手机号授权微信返回sessionData：" + session.toString());

            // 解密用户信息
            WxMaPhoneNumberInfo wxMpUser = wxMaService.getUserService().getPhoneNoInfo(session.getSessionKey(), fullUserInfo.getEncryptedData(), fullUserInfo.getIv());

            WxUserEntity user = userService.getById(userId);
            user.setPhone(wxMpUser.getPurePhoneNumber());
            userService.updateById(user);
            return RestResponse.success("手机号授权成功");
        } catch (WxErrorException e) {
            log.error("手机号授权失败：" + e.getMessage());
            return RestResponse.error("手机号授权失败");
        }
    }

    /**
     * 微信公众号登录
     *
     * @return RestResponse
     */
    @IgnoreAuth
    @PostMapping("loginByMp")
    @ApiOperation(value = "微信公众号登录", notes = "根据微信code登录，每一个code只能使用一次")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", name = "jsonObject", value = "JSON格式参数", examples = @Example({
                    @ExampleProperty(mediaType = "code", value = "oxaA11ulr9134oBL9Xscon5at_Gc")
            }), required = true, dataType = "string")
    })
    public RestResponse loginByMp(@RequestBody JSONObject jsonObject) {
        String code = jsonObject.getString("code");

        AbstractAssert.isBlank(code, "登录失败：code为空");

        Map<String, Object> map = new HashMap<>(8);
        try {
            WxOAuth2AccessToken auth2AccessToken = wxMpService.getOAuth2Service().getAccessToken(code);

            String openId = auth2AccessToken.getOpenId();

            //获取微信用户信息
            WxOAuth2UserInfo wxMpUser = wxMpService.getOAuth2Service().getUserInfo(auth2AccessToken, null);

            //保存或者更新
            WxUserEntity user = userService.getOne(new QueryWrapper<WxUserEntity>().eq("UNION_ID", wxMpUser.getUnionId()));
            if (user == null) {
                user = userService.getOne(new QueryWrapper<WxUserEntity>().eq("OPENID", wxMpUser.getOpenid()));
            }
            // 没有数据即新注册用户
            if (user == null) {
                user = new WxUserEntity();
                user.setAllIntegral(0);
                user.setIntegral(0);
                user.setRegisterTime(new Date());
                user.setRegisterIp(getClientIp());
            }
            user.setLastLoginTime(new Date());
            user.setLastLoginIp(getClientIp());
            user.setUnionId(wxMpUser.getUnionId());
            user.setOpenid(wxMpUser.getOpenid());
            user.setNickname(wxMpUser.getNickname());
            user.setHeadImgUrl(wxMpUser.getHeadImgUrl());

            Boolean subscribe = wxMpService.getUserService().userInfo(openId).getSubscribe();
            user.setSubscribe(subscribe);

            userService.saveOrUpdate(user);

            //生成token
            String token = jwtUtils.generateToken(user.getId());

            map.put("token", token);
            map.put("userInfo", user);
            map.put("userId", user.getId());
        } catch (WxErrorException e) {
            log.error("登录失败：" + e.getMessage());
            return RestResponse.error("登录失败");
        }

        return RestResponse.success(map);
    }

    /**
     * APP端微信登录
     */
    @IgnoreAuth
    @PostMapping("AppLoginByWx")
    @ApiOperation(value = "APP端微信登录", notes = "APP端微信登录")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "body", name = "jsonObject", value = "JSON格式参数", examples = @Example({
                    @ExampleProperty(mediaType = "userInfo", value = "uni.login返回的authResult信息，JSON格式参数")
            }), required = true, dataType = "string")
    })
    public RestResponse appLoginByWx(@RequestBody JSONObject jsonObject) {
        WxOAuth2AccessToken auth2AccessToken = null;

        if (null != jsonObject.get(USER_INFO)) {
            auth2AccessToken = jsonObject.getObject(USER_INFO, WxOAuth2AccessToken.class);
        }
        AbstractAssert.isNull(auth2AccessToken, "登录失败：userInfo为空");

        try {
            //获取微信用户信息
            WxOAuth2UserInfo wxMpUser = wxMpService.getOAuth2Service().getUserInfo(auth2AccessToken, null);

            WxUserEntity user = userService.getOne(new QueryWrapper<WxUserEntity>().eq("UNION_ID", wxMpUser.getUnionId()));
            // 没有数据即新注册用户
            if (user == null) {
                user = new WxUserEntity();
                user.setAllIntegral(0);
                user.setIntegral(0);
                user.setRegisterTime(new Date());
                user.setRegisterIp(getClientIp());
            }
            user.setLastLoginTime(new Date());
            user.setLastLoginIp(getClientIp());
            user.setUnionId(wxMpUser.getUnionId());
            user.setNickname(wxMpUser.getNickname());
            user.setHeadImgUrl(wxMpUser.getHeadImgUrl());
            userService.saveOrUpdate(user);

            String token = jwtUtils.generateToken(user.getId());

            if (StringUtils.isNullOrEmpty(token)) {
                log.error("登录失败：token生成异常");
                return RestResponse.error("登录失败");
            }
            return RestResponse.success().put("token", token).put("userInfo", user).put("userId", user.getId());
        } catch (WxErrorException e) {
            log.error("登录失败：" + e.getMessage());
            return RestResponse.error("登录失败");
        }
    }

    /**
     * 创建调用jsapi时所需要的签名
     *
     * @return RestResponse
     */
    @IgnoreAuth
    @GetMapping("/createJsapiSignature")
    @ApiOperation(value = "创建调用jsapi时所需要的签名", notes = "创建调用jsapi时所需要的签名")
    @ApiImplicitParams({
            @ApiImplicitParam(required = true, paramType = "query", name = "url", value = "url", dataType = "string")
    })
    public RestResponse createJsapiSignature(String url) {
        WxJsapiSignature data = null;
        try {
            data = wxMpService.createJsapiSignature(url);
        } catch (WxErrorException e) {
            e.printStackTrace();
        }
        Map<String, Object> map = new HashMap<>(8);
        map.put("data", data);

        return RestResponse.success(map);
    }

    @IgnoreAuth
    @GetMapping("/{code}")
    public RestResponse loginByCode(@PathVariable String code) {
        try {
            WxMaJscode2SessionResult session = wxMaService.getUserService().getSessionInfo(code);

            String openid = session.getOpenid();
            String unionid = session.getUnionid();
            WxUserEntity user = userService.getOne(new QueryWrapper<WxUserEntity>().eq("UNION_ID", session.getUnionid()));
            if (user == null) {
                user = userService.getOne(new QueryWrapper<WxUserEntity>().eq("MA_OPENID", openid));
            }

            if (Objects.isNull(user)) {
                // 用户不存在，新增用户
                user = new WxUserEntity();
                user.setAllIntegral(0);
                user.setIntegral(0);
                user.setRegisterTime(new Date());
                user.setRegisterIp(getClientIp());
                user.setLastLoginTime(new Date());
                user.setLastLoginIp(getClientIp());
                user.setUnionId(unionid);
                user.setMaOpenid(openid);
                userService.save(user);
            }

            String token = jwtUtils.generateToken(user.getId());

            if (StringUtils.isNullOrEmpty(token)) {
                log.error("登录失败：token生成异常");
                return RestResponse.error("登录失败");
            }
            long expire = System.currentTimeMillis() + jwtUtils.getExpire();
            return RestResponse.success().put("token", token).put("userInfo", user).put("userId", user.getId()).put("expire", expire);
        } catch (WxErrorException e) {
            log.error("登录失败", e);
            return RestResponse.error("登录失败");
        }
    }

    /**
     * 获取当前登录用户
     *
     * @param loginUser 登录用户
     * @return RestResponse
     */
    @GetMapping("/getUserInfo")
    @ApiOperation(value = "获取当前登录用户", notes = "根据token获取当前登录用户")
    @ApiImplicitParam(paramType = "header", name = "token", value = "用户token", required = true, dataType = "string")
    public RestResponse getUserInfo(@LoginUser WxUserEntity loginUser) {
        return RestResponse.success().put("userInfo", loginUser).put("userId", loginUser.getOpenid());
    }

}
